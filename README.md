# Todo Vue Js Web

## Development usage
```
npm install
npm run serve
```

## Test
```
cd tests
npm install
npm test
```

## Docker
```
docker-compose up --build
```

## API
[Todo API](https://github.com/srrozden/todo-api)  