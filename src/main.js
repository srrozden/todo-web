import Vue from "vue";
import App from "./App.vue";
import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "http://localhost:3000"
});


Vue.prototype.$axios = axiosInstance;
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount("#app");
