# Getting Started with Gauge

This is an executable specification file. This file follows markdown syntax. Every heading in this file denotes a scenario. Every bulleted point denotes a step.
To execute this specification, use
	npm test

## Search Taiko Repository

* Go to page
* Add New Data "buy some milk"
* Control "buy some milk"
* Add New Data "enjoy the assignment"
* Control "enjoy the assignment"
* Check "buy some milk"
* Check "buy some milk"
* Add New Data "rest for a while"
* Add New Data "drink water"
* Remove "rest for a while"
