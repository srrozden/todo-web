/* globals gauge*/
"use strict";
const { openBrowser, closeBrowser, write, goto, press, screenshot, text, focus, textBox, click } = require('taiko');
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
    await openBrowser({ headless: headless })
});

afterSuite(async () => {
    await closeBrowser();
});

gauge.screenshotFn = async function() {
    return await screenshot({ encoding: 'base64' });
};

step("Go to page", async () => {
    await goto('http://localhost:3000');
});

step("Add New Data <content>", async (content) => {
    await focus(textBox({id: 'newTodo'}))
    await write(content);
    await press('Enter');
});

step("Control <content>", async (content) => {
    assert.ok(await text(content).exists());
});

step("Check <content>", async (content) => {
    await click(content);
});

step("Remove <content>", async (content) => {
    await click(content);
    await press('Tab');
    await press('Tab');
    await press('Enter');
});

